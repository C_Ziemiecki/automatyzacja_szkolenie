import signal
import sys, os, json
from quiz.app import app

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5700, debug=False)