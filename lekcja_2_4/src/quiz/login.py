from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, login_required, current_user, login_user, logout_user, UserMixin
from .models import db, User
login = Blueprint('login', __name__)

class Login:
    def __init__(self, db):
        self.db = db
    login_manager = LoginManager()
    login_manager.login_view = 'loger.login'

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))


@Login.login_manager.unauthorized_handler
def unauthorized_callback():
    return render_template('index.html')

@login.route('/log_in', methods=['POST'])
def log_in():
    name = request.form.get('login')
    password = request.form.get('password')
    user = User.query.filter_by(nick=name).first()
    if user:
        if not check_password_hash(user.password, password):
            return redirect(url_for('index'))
        login_user(user)
        return redirect(url_for('main.question_page'))
    else:
        new_user = User(nick=name, password=generate_password_hash(password, method='sha256'))
        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user)
        return redirect(url_for('main.question_page'))