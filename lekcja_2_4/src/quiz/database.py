from flask import Blueprint

from .models import Rank, User

login = Blueprint('login', __name__)


def register_game(db, user, category, level, success):
    game = Rank(nick=user.nick, category=category, level=level, success=success)
    db.session.add(game)
    db.session.commit()


def print_leader_board():
    queries = User.query.filter(True)
    leader_board = [{'nick': user.nick, 'points': calc_points(user)} for user in queries]
    return leader_board


def calc_points(user):
    points = Rank.query.filter_by(nick=user.nick, success=True)
    sum = 0
    for point in points:
        sum += point.level
    return sum
