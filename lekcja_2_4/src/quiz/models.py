from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    password = db.Column(db.String(100))
    nick = db.Column(db.String(1000), unique=True)


class Rank(db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    nick = db.Column(db.String(1000))
    category = db.Column(db.String(1000))
    level = db.Column(db.Integer())
    success = db.Column(db.Boolean())