let extendRequestExecute = function (address, onloadFunction, ...args) {
    try {
        let request = new XMLHttpRequest();
        request.open("GET", address, true);
        request.send();
        request.onload = function () {
            onloadFunction(request, ...args)
        }

    } catch {
        console.debug('error')
    }
}
let print_success = (event) => {
    alert(JSON.parse(event.response) ? 'SUCCESS' : "FALSE")

    location.reload();
}
let print_question = (event) => {
    let response = JSON.parse(event.response)
    document.getElementById('question_content').innerHTML = '[' + response['difficulty'] + ']:  ' + response['question']
}
addEventListener("DOMContentLoaded", () => {

    let level = document.createElement("select");
    let easy = document.createElement("option");
    let medium = document.createElement("option");
    let hard = document.createElement("option");
    easy.value = "1";
    easy.text = "easy";

    medium.value = "2";
    medium.text = "medium";

    hard.value = "3";
    hard.text = "hard";

    level.add(easy, null);
    level.add(medium, null);
    level.add(hard, null);

    let input = document.createElement("input");
    input.type = "range";
    input.setAttribute('step', 1);
    input.setAttribute('min', 2);
    input.setAttribute('max', 10);
    let button = document.createElement('button');
    button.textContent = 'LOAD'
    document.getElementById('question-set').append(level, document.getElementById('submit'));
    document.getElementById('question-set').insertBefore(input, document.getElementById('submit'));
    document.getElementById('question-set').insertBefore(button, document.getElementById('submit'));
    let question_content = document.createElement('div')
    question_content.id = 'question-set-content';
    document.getElementById('question-set').insertBefore(question_content, document.getElementById('submit'));
    button.addEventListener('click', event => {
        event.preventDefault();
        while (document.getElementById('question-set-content').getElementsByTagName('p')[0]!=undefined){
            document.getElementById('question-set-content').getElementsByTagName('p')[0].remove()
        }
        while (document.getElementById('question-set-content').getElementsByTagName('div')[0]!=undefined){
            document.getElementById('question-set-content').getElementsByTagName('div')[0].remove()
        }
        extendRequestExecute(`new_multiple_question/${level.value}/${input.value}`, event => {
            let response = JSON.parse(event.response);
            response.forEach((element, index) => {
                let content = document.createElement('p')
                content.innerHTML = element['question']
                document.getElementById('question-set-content').append(content);
                let answers = document.createElement('div');
                let prawda = document.createElement('input')
                prawda.type = 'radio'
                prawda.value = true
                prawda.dataset.id = index
                prawda.setAttribute('name',`quiz_${index}`);
                answers.append(prawda)
                let prawdaLabel = document.createElement('label');
                prawdaLabel.textContent = 'PRAWDA'
                answers.append(prawdaLabel)
                let falsz = document.createElement('input')
                falsz.type = 'radio'
                falsz.value = false
                falsz.dataset.id = index
                falsz.setAttribute('name',`quiz_${index}`);
                answers.append(falsz)
                let falszLabel = document.createElement('label');
                falszLabel.textContent = 'FALSZ'
                answers.append(falszLabel)
                document.getElementById('question-set-content').append(answers);
            })

        })
    })
    document.getElementById('submit').addEventListener('click',event=>{
        let answers = []
        Array.from(document.querySelectorAll('input[type="radio"]:checked')).forEach(element=>{answers.push(element.value=='true'?true:false)})
        let element = {'answers': answers, 'level': parseInt(level.value)}
        let json = JSON.stringify(element)
        extendRequestExecute(`send_complex_response/${json}`, event=>{
            console.log(event.response)
        })
    })
document.getElementById('rank').addEventListener('click',event=>{
    while (document.getElementById('rank-set').firstChild){
        document.getElementById('rank-set').firstChild.remove()
    }
    event.preventDefault();
    extendRequestExecute('get_leader_board',event=>{
        let response = JSON.parse(event.response)
        response.forEach(element =>{
        let pElement = document.createElement('p')
        pElement.innerHTML = `<b>${element['nick']}</b> ${element['points']}`
        document.getElementById('rank-set').append(pElement)
    })
    })
})
}, true);