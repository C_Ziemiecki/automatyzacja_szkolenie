import json

import requests
from flask import render_template, Blueprint, Response, abort
from flask_login import current_user
from jsonschema import validate

from .database import register_game, print_leader_board
from .models import db

main = Blueprint('main', __name__)

schema = {
    "type": "object",
    "properties": {
        "answers": {"type": "array", "minItems": 2,
                    "maxItems": 10, "items": {
                "type": "boolean"
            }},
        "level": {"type": "number"},
    },
}


class Question():
    def __init__(self):
        self.current = None
        self.multiple_current = None
        self.level = 0

    def check_result(self, response):
        return response == self.current['correct_answer']

    def check_multiple_result(self, response):
        points = -int(response['level'])

        for answer in response['answers']:
            if str(answer) != self.multiple_current.pop(0)['correct_answer']:
                return False, 0
            else:
                points += 2 * int(response['level'])
        return True, points


    def load_question(self, level):
        question.current = \
            requests.get(
                url=f"https://opentdb.com/api.php?amount=1&difficulty={game_levels[level]}&type=boolean").json()[
                "results"][0]
        question.level = int(level)
        return question.current

    def load_multiple_question(self, level, number):
        question.multiple_current = \
            requests.get(
                url=f"https://opentdb.com/api.php?amount={number}&difficulty={game_levels[level]}&type=boolean").json()[
                "results"]
        question.level = int(level)
        return question.multiple_current


game_levels = {'1': 'easy', '2': 'medium', '3': 'hard'}
question = Question()


@main.route('/new_question/<level>')
def new_question(level):
    return Response(json.dumps(question.load_question(level)), mimetype='application/json')


@main.route('/new_multiple_question/<level>/<number>')
def new_multiple_question(level, number):
    return Response(json.dumps(question.load_multiple_question(level, number)), mimetype='application/json')


@main.route('/question_page')
def question_page():
    return render_template('quiz.html', current_user=current_user.nick.capitalize())

@main.route('/multiple_question_page')
def multiple_question_page():
    return render_template('multiple_quiz.html', current_user=current_user.nick.capitalize())

@main.route('/send_response/<response>')
def send_response(response):
    try:
        response_in_format = str(json.loads(response))
    except:
        abort(400)
    success = question.check_result(response_in_format)
    register_game(db, current_user, question.current['category'], question.level, success)
    return Response(json.dumps(success), mimetype='application/json')


@main.route('/send_complex_response/<response>')
def send_complex_response(response):
    try:
        response_in_format = json.loads(response)
    except:
        abort(400)
    try:
        validate(instance=response_in_format, schema=schema, )
    except:
        abort(401)
    success, points = question.check_multiple_result(response_in_format)
    register_game(db, current_user, 'multiple', points, success)
    return Response(json.dumps(success), mimetype='application/json')


@main.route('/get_leader_board')
def get_leader_board():
    return Response(json.dumps(print_leader_board()), mimetype='application/json')
