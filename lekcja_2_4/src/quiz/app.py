from flask import Flask, render_template
import os
from .models import db
template_dir = os.path.abspath('src/quiz/templates')
static_dir = os.path.abspath('src/quiz/static')
app = Flask(__name__, template_folder=template_dir, static_folder=static_dir, static_url_path='')
app.config['SECRET_KEY'] = 'secret-key-goes-here'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
from .login import Login, login as login_blueprint
db.init_app(app)

loger = Login(db)
loger.login_manager.init_app(app)
app.register_blueprint(login_blueprint, url_prefix='/login')

from .api import main as main_blueprint
app.register_blueprint(main_blueprint, url_prefix='/main')

db.create_all(app=app)
@app.route('/')  # Strona glowna
def index():
    return render_template('index.html')