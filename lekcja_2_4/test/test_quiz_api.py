from unittest.mock import Mock

import pytest
from quiz.api import Question
from quiz.app import app
from quiz.database import calc_points, print_leader_board
from quiz.models import User, Rank
from quiz.models import db
from webtest import TestApp
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

def test_api_is_wsgi_app():
    assert hasattr(app, 'wsgi_app')


def test_api_bad_endpoint():
    assert app.test_client().get('/new_question/2').status_code == 404


def test_api_get_medium_question():
    response = app.test_client().get('main/new_question/2')
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert 'difficulty' in response.json
    assert response.json['difficulty'] == 'medium'


def test_api_get_multiple_hard_question():
    response = app.test_client().get('main/new_multiple_question/3/5')
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert len(response.json) == 5
    assert 'difficulty' in response.json[2]
    assert response.json[2]['difficulty'] == 'hard'


def test_quiz_page_renderer(monkeypatch):
    monkeypatch.setattr('quiz.api.current_user', Mock(nick=Mock(return_value='Cezary')))
    response = app.test_client().get('main/question_page')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'


def test_multiple_quiz_page_renderer(monkeypatch):
    monkeypatch.setattr('quiz.api.current_user', Mock(nick=Mock(return_value='Cezary')))
    response = app.test_client().get('main/multiple_question_page')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'


def test_quiz_page_bad_request():
    response = app.test_client().post('main/question_page')
    assert response.status_code == 405


def test_index_page_renderer():
    response = app.test_client().get('/')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'


@pytest.fixture
def user_mock():
    return User(nick='test')


def test_calc_points(user_mock, monkeypatch):
    monkeypatch.setattr('quiz.database.Rank', Mock(
        query=Mock(filter_by=Mock(return_value=[Mock(level=1), Mock(level=2), Mock(level=1), Mock(level=3)]))))
    assert calc_points(user_mock) == 7


def test_print_leader_board(monkeypatch):
    monkeypatch.setattr('quiz.database.User', Mock(query=Mock(filter=Mock(
        return_value=[Mock(nick='Cezary'), Mock(nick='Anna')]))))
    monkeypatch.setattr('quiz.database.calc_points', Mock(side_effect=[3, 5]))
    assert print_leader_board() == [{'nick': 'Cezary', 'points': 3}, {'nick': 'Anna', 'points': 5}]


def test_api_send_non_json_response():
    response = app.test_client().get('main/send_response/1,2,3,4')
    assert response.status_code == 400


def test_api_send_correct_response(monkeypatch):
    monkeypatch.setattr('quiz.api.question.check_result', lambda success: True)
    monkeypatch.setattr('quiz.api.register_game', Mock())
    response = app.test_client().get('main/send_response/true')
    assert response.status_code == 200


def test_api_get_leader_board(monkeypatch):
    monkeypatch.setattr('quiz.api.print_leader_board', Mock(return_value={'Cezary': 3, 'Anna': 5}))
    response = app.test_client().get('main/get_leader_board')
    assert response.status_code == 200


def test_login_user(monkeypatch):
    monkeypatch.setattr('quiz.login.request', Mock(form=Mock(get=Mock(side_effect=['Anon', 'password']))))
    response = app.test_client().post('login/log_in')
    assert response.status_code == 302
    assert b'Redirecting...' in response.data


def test_login_new_user(monkeypatch):
    monkeypatch.setattr('quiz.login.request', Mock(form=Mock(get=Mock(side_effect=['Anon', 'password']))))
    # monkeypatch.setattr('quiz.login.User', Mock(query=Mock(filter_by=Mock(first=Mock(return_value=False)))))
    monkeypatch.setattr('quiz.login.User',
                        Mock(query=Mock(filter_by=Mock(return_value=Mock(first=Mock(return_value=False))))))
    monkeypatch.setattr('quiz.login.db.session', Mock(add=Mock(), commit=Mock()))
    monkeypatch.setattr('quiz.login.login_user', Mock())
    response = app.test_client().post('login/log_in')
    assert response.status_code == 302
    assert b'Redirecting...' in response.data


@pytest.fixture
def question_mock():
    return {'correct_answer': False}


@pytest.fixture
def multiple_question_mock():
    return [{'correct_answer': 'True'}, {'correct_answer': 'False'}, {'correct_answer': 'False'}]


def test_question_check_result(question_mock):
    question = Question()
    question.current = question_mock
    assert question.check_result(False) == True


def test_question_multiple_check_result(multiple_question_mock):
    question = Question()
    question.multiple_current = multiple_question_mock
    success, points = question.check_multiple_result({'answers': [True, False, False], 'level': 3})
    assert success == True
    assert points == 15


def test_question_bad_multiple_check_result(multiple_question_mock):
    question = Question()
    question.multiple_current = multiple_question_mock
    success, points = question.check_multiple_result({'answers': [True, True, False], 'level': 1})
    assert success == False


def test_api_send_bad_schema_response(monkeypatch):
    response = app.test_client().get('main/send_complex_response/{"answers": [true], "level": 2}')
    assert response.status_code == 401


def test_api_send_bad_json_response(monkeypatch):
    response = app.test_client().get('main/send_complex_response/{"anue], "level": 2}')
    assert response.status_code == 400


def test_api_send_good_json_response(monkeypatch):
    monkeypatch.setattr('quiz.api.register_game', Mock())
    response = app.test_client().get('main/send_complex_response/%7B%22answers%22:[true,false],%22level%22: 2%7D')
    assert response.status_code == 200


# @pytest.fixture
# def testapp(app, db):
#     with app.app_context():
#         db.create_all()
#     return TestApp(app)
#
#
# class TestRegistration:
#
#     def test_can_register(self, testapp):
#         user_count = User.query.count()
#         response = testapp.get("/")
#         response = response.click("Create account")  # ➜1
#         form = response.forms["registrationForm"] # ➜2
#         form["username"] = "foobar"
#         form["email"] = "foo@bar.com"
#         form["password"] = "secret"
#         form["confirm"] = "secret"
#         res = form.submit().follow()  # ➜3
#         assert res.status_code == 200
#         assert User.query.count() == user_count + 1 # ➜4




@pytest.fixture
def testapp():
    app.config['SECRET_KEY'] = 'secret-key-goes-here'
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///tests.db"
    db.app = app
    db.init_app(app)
    with app.app_context():
        db.create_all()
        yield TestApp(app)
    User.query.filter_by(nick="footbar").delete()
    Rank.query.filter_by(nick="footbar").delete()
    db.session.commit()

def test_can_register(testapp):
    with app.app_context():
        user_count = User.query.count()
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "secret"
        res = form.submit().follow()
        assert res.status_code == 200
        assert User.query.count() == user_count+1
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "dekret"
        res = form.submit().follow()
        assert User.query.count() == user_count + 1
        assert 'registrationForm' in res

def point_read(json):
    for element in json:
        if element.get('nick') == 'footbar':
            return element.get('points')

def test_can_answer(testapp):
    with app.app_context():
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "secret"
        form.submit().follow()
        response = testapp.get("/main/question_page")
        response = response.click(linkid='rank')
        first_response = response
        response = testapp.get('/main/new_question/1')
        correct_answer = str(response.json['correct_answer']).lower()
        response = testapp.get(f'/main/send_response/{correct_answer}')
        response = testapp.get("/main/question_page")
        response = response.click(linkid='rank')
        points2 = point_read(response.json)
        assert first_response.status_code == 200
        assert any(element.get('nick') == 'footbar' for element in first_response.json)
        assert points2 == 1
