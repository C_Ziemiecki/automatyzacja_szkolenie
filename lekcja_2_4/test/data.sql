INSERT INTO user (id, nick, password)
VALUES
  (0,'Cezary', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f'),
  (1,'Anna', 'pbkdf2:sha256:50000$kJPKsz6N$d2d4784f1b030a9761f5ccaeeaca413f27f2ecb76d6168407af962ddce849f79');

INSERT INTO rank (id, nick, category, level, success)
VALUES
  (0,'Cezary', 'Maths', 1, 1),
       (1,'Anna', 'Maths', 2, 1),
       (2,'Anna', 'Maths', 3, 1),
       (3,'Cezary', 'Maths', 1, 1),
       (4,'Cezary', 'Maths', 1, 1),
       (2,'Anna', 'Maths', 3, 0);