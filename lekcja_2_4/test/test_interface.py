from unittest.mock import Mock

import pytest
from quiz.api import Question
from quiz.app import app
from quiz.database import calc_points, print_leader_board
from quiz.models import User, Rank
from quiz.models import db
from webtest import TestApp
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait  # 1
from selenium.webdriver.support import expected_conditions as EC
import json

@pytest.fixture
def browser():
    options = Options()
    # options.add_argument('--headless')
    chrome = Chrome(chrome_options=options)
    chrome.implicitly_wait(3)
    chrome.maximize_window()
    chrome.get('http://192.168.42.181:5700')
    yield chrome
    chrome.quit()


def test_registration_form_click_to_username(browser):  # 2
    nick_input = browser.find_element_by_id('nick-input')
    nick_input.click()
    assert WebDriverWait(browser, timeout=3).until(  # 8
        lambda d: d.find_element_by_id('nick-input')  # 9
    ).get_attribute('value') == 'test'  # 10


def test_registration_form_with_answer_response(browser):  # 2
    link = browser.find_element_by_id('login')
    pass_input = browser.find_element_by_id('pass-input')
    pass_input.send_keys("tester")
    nick_input = browser.find_element_by_id('nick-input')
    nick_input.send_keys("tester")
    link.click()
    assert WebDriverWait(browser, timeout=3).until(lambda d: d.find_element_by_id('rank')).get_attribute('class') == ""
    medium = browser.find_element_by_id('medium')
    medium.click()
    time.sleep(3)
    assert 'medium' in browser.find_element_by_id('question_content').text
    prawda = browser.find_element_by_id('value_2')
    prawda.click()
    time.sleep(2)
    submit = browser.find_element_by_id('submit')
    submit.click()
    alert = WebDriverWait(browser, 3).until(EC.alert_is_present(), '').text
    assert  alert == 'SUCCESS' or alert == 'FALSE'

def test_check_rank(browser):  # 2
    link = browser.find_element_by_id('login')
    pass_input = browser.find_element_by_id('pass-input')
    pass_input.send_keys("tester")
    nick_input = browser.find_element_by_id('nick-input')
    nick_input.send_keys("tester")
    link.click()
    assert WebDriverWait(browser, timeout=3).until(lambda d: d.find_element_by_id('rank')).get_attribute('class') == ""
    rank = browser.find_element_by_id('rank')
    rank.click()
    content = browser.find_element_by_tag_name('pre').text
    assert any('tester' == element.get('nick') for element in json.loads(content))