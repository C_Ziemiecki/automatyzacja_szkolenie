from behave import fixture, use_fixture

from behave import *
from quiz.app import app
from quiz.models import User, Rank
from quiz.models import db
from webtest import TestApp


@fixture
def testapp(context, *args, **kwargs):
    app.config['SECRET_KEY'] = 'secret-key-goes-here'
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///tests.db"
    app.testing = True
    context.client = TestApp(app)
    db.app = app
    db.init_app(app)
    with app.app_context():
        db.create_all()
        yield context.client
    User.query.filter_by(nick="footbar").delete()
    Rank.query.filter_by(nick="footbar").delete()
    db.session.commit()


def before_feature(context, feature):
    use_fixture(testapp, context)
