Feature: self-service user answer
  Users want to be answer single question

  Background: Login user
    Given user navigated to /
    When fill good registration form for user "jdoe"
    Given user navigated to /main/question_page

  Scenario: user answer correctly
    Given user: "jdoe" check rank is equal to 0
    When user good answer question
    Given user: "jdoe" check rank is equal to 1
    Then delete user: "jdoe"


  Scenario: user answer badly
    Given user: "jdoe" check rank is equal to 0
    When user bad answer question
    Given user: "jdoe" check rank is equal to 0
    Then delete user: "jdoe"
