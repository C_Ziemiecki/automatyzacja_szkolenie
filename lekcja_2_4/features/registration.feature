Feature: self-service user registration
  Users want to be able to register themselves

  Background: registration form is open
    Given user navigated to /

  Scenario: user already exists
    Given user "jdoe" exists
    When fill bad registration form for user "jdoe"
    Then registration fails with error "Username already registered"
    Then delete user: "jdoe"