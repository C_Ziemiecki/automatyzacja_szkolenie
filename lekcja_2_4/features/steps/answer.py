from behave import *

from quiz.app import app
from quiz.models import User
from quiz.models import db
from werkzeug.security import generate_password_hash

@given(u'user: "{username}" check rank is equal to {points:d}')
def step_impl(context, username, points):
    # add the new user to the database
    response = context.response.click(linkid='rank')
    assert response.status_code == 200
    assert any(element.get('nick') == 'jdoe' for element in response.json)
    assert point_read(response.json,username) == points

@when(u'user good answer question')
def step_impl(context):
    response = context.client.get('/main/new_question/1')
    correct_answer = str(response.json['correct_answer']).lower()
    response = context.client.get(f'/main/send_response/{correct_answer}')
    assert response.status_code == 200

@when(u'user bad answer question')
def step_impl(context):
    response = context.client.get('/main/new_question/1')
    incorrect_answer = str(response.json['incorrect_answers'][0]).lower()
    response = context.client.get(f'/main/send_response/{incorrect_answer}')
    assert response.status_code == 200

def point_read(json,username):
    for element in json:
        if element.get('nick') == username:
            return element.get('points')

def test_can_answer(testapp):
    with app.app_context():
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "secret"
        form.submit().follow()
        response = testapp.get("/main/question_page")
        response = response.click(linkid='rank')
        first_response = response
        response = testapp.get('/main/new_question/1')
        correct_answer = str(response.json['correct_answer']).lower()
        response = testapp.get(f'/main/send_response/{correct_answer}')
        response = testapp.get("/main/question_page")
        response = response.click(linkid='rank')
        points2 = point_read(response.json)
        assert first_response.status_code == 200
        assert any(element.get('nick') == 'footbar' for element in first_response.json)
        assert points2 == 1

