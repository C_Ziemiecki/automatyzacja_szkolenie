from behave import *

from quiz.app import app
from quiz.models import User, Rank
from quiz.models import db
from werkzeug.security import generate_password_hash


@given(u'user navigated to {endpoint}')
def navigate_to(context, endpoint):
    context.response = context.client.get(endpoint)
    print(context.response)


@given(u'user "{username}" exists')
def create_user(context, username):
    # add the new user to the database
    user = User.query.filter_by(nick=username).first()
    if not user:
        new_user = User(nick=username, password=generate_password_hash('test', method='sha256'))
        db.session.add(new_user)
        db.session.commit()


@when(u'fill bad registration form for user "{username}"')
def step_impl(context, username):
    form = context.response.forms["registrationForm"]
    form["login"] = username
    form["password"] = "secret"
    context.response = form.submit()


@when(u'fill good registration form for user "{username}"')
def step_impl(context, username):
    form = context.response.forms["registrationForm"]
    form["login"] = username
    form["password"] = "test"
    context.response = form.submit()


@then(u'registration fails with error "Username already registered"')
def step_impl(context):
    assert context.response.status_code == 302
    assert "Redirecting..." in context.response


@then(u'delete user: "{username}"')
def step_impl(context, username):
    assert User.query.filter_by(nick=username).first()
    User.query.filter_by(nick=username).delete()
    Rank.query.filter_by(nick=username).delete()
    db.session.commit()
    assert not User.query.filter_by(nick=username).first()
    assert not Rank.query.filter_by(nick=username).first()

def test_can_register(testapp):
    with app.app_context():
        user_count = User.query.count()
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "secret"
        res = form.submit().follow()
        assert res.status_code == 200
        assert User.query.count() == user_count + 1
        response = testapp.get("/")
        form = response.forms["registrationForm"]
        form["login"] = "footbar"
        form["password"] = "dekret"
        res = form.submit().follow()
        assert User.query.count() == user_count + 1
        assert 'registrationForm' in res
