import math

# FUNKCJA NADPROGRAMOWA
# Wcześniej psotawiony warunek sprawdza czy ma rozmiar 9 elementów
# def list_to_square_array(old_list):
# 	side=math.sqrt(len(old_list))
# 	if side.is_integer():
# 		return [old_list[x:x+int(side)] for x in range(0, len(old_list), int(side))]
# 	return old_list

def tic_tac_toe_winner(board):
	"""
    >>> from bootcamp3 import tic_tac_toe_winner
    >>> tic_tac_toe_winner('')
    Traceback (most recent call last):
        raise ValueError()
    ValueError
    >>> tic_tac_toe_winner('XXXOOOXOX')
    Traceback (most recent call last):
    	raise TypeError()
    TypeError
    
    """
	def check_compatibility(board):
		if isinstance(board,str):
			x_count = board.count('X')
			y_count = board.count('O')
			empty_count = board.count(' ')
			# Warunek sprawdzający czy występują inne niz dozwolone znaki
			#znaki lub czy tablica ma odpowiedni wymiar 
			#  len(board)!=9 nie spełniało pierwszego warunku
			if x_count+y_count+empty_count!=9:    
				pass
			elif abs(x_count-y_count)>1:
				pass
			elif x_count >5 or y_count>5:
				pass
			elif x_count+y_count<5:
				pass
			else:
				return True
		return False

	def check_winners(candidates):
		if len(candidates)==0 or candidates[0]==' ':
			return 'None'
		else:
			for sign in range(1,len(candidates)):
				if candidates[0]!=candidates[sign]:
					return 'Error'
		return candidates[0]

	if check_compatibility(board):
		new_board = [board[x:x+3] for x in range(0, 9, 3)]
		candidates = []
		for x in range(3):
			if new_board[x][0] == new_board[x][1] == new_board[x][2]:
				candidates.append(new_board[x][0])
			if new_board[0][x] == new_board[1][x] == new_board[2][x]:
				candidates.append(new_board[0][x])
		if new_board[0][0] == new_board[1][1] == new_board[2][2]:
			candidates.append(new_board[0][0])
		if new_board[0][2] == new_board[1][1] == new_board[2][0]:
			candidates.append(new_board[2][0])
		winner = check_winners(candidates)
		if winner in ['X','O','None']:
			return winner
		raise TypeError()
	raise ValueError()




test_cases = {
	'XXXXOOXOX': ValueError,
	'XXX  OXOX': ValueError,
	'OXXXO X O': 'O',
	'XXXXO XOX': ValueError,
	'XXOXXOOOX': 'X',
	'XOXXOOXOX': TypeError,
	'XXXXOOXOX': ValueError,
	'         ': ValueError,
	'XXXXXXXXX': ValueError,
	'XXXOOOXOX': TypeError,
	'OXOXOOXOX': 'None',
	'XOOOOOXOX': ValueError,
	'XOOOOOXOX': ValueError,
	'XOXOOOXOX': 'O',
	'XXXOOOXXX': ValueError,
	'XXXOOOrXX': ValueError,
	'XXXOOOrOX': ValueError,
	'XXXOOOXX': ValueError,
	'XOO   XXO': 'None',
	'XXXOOOXXXOO': ValueError,
	12: ValueError,
	'': ValueError,
	'xoxoxoxox': ValueError
}
if __name__ == "__main__":
	# import doctest
	# doctest.testmod()
	count = 0
	for board, expectation in test_cases.items():
		count+=1
		try:
			response = tic_tac_toe_winner(board)
			assert response == expectation, f'Expected {repr(expectation)} for {repr(board)} got {repr(response)}'
		except ValueError:
			assert expectation == ValueError, f'Expected {repr(expectation)} for {repr(board)} got {ValueError}'
		except TypeError:
			assert expectation == TypeError, f'Expected {repr(expectation)} for {repr(board)} got {TypeError}'
