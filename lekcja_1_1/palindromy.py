def is_palindrome(input, check_size):
	if isinstance(input, dict):
		raise TypeError
	try:
		joined = ''.join(input)
	except:
		raise TypeError
	string = str(joined) if check_size else str(joined).lower()
	# przyjmujemy palindormy o romiarze conajmniej trzech znaków
	if len(string)>2 and string == string[::-1]:
		return True
	return False


def tester_function():
	return "kajak"




test_cases = [
# sprawdzanie wielkosci liter
{'string': 'oko', 'check_size': True, 'output': True},
{'string': 'okO', 'check_size': True, 'output': False},
{'string': 'Oko', 'check_size': True, 'output': False},
{'string': 'okokajakoko', 'check_size': True, 'output': True},
{'string': 'katastrofa', 'check_size': True, 'output': False},
{'string': 'kamilŚlimak', 'check_size': True, 'output': True},
{'string': 'Holandia', 'check_size': False, 'output': False},
{'string': 8, 'check_size': True, 'output': TypeError},
{'string': '', 'check_size': True, 'output': False},
{'string': 234, 'check_size': True, 'output': TypeError},
{'string': '23432', 'check_size': True, 'output': True},
{'string': ['a','b','a'], 'check_size': True, 'output': True},
{'string': ['a','b','A'], 'check_size': True, 'output': False},
{'string': (234,'g'), 'check_size': True, 'output': TypeError},
{'string': ('a','b','a'), 'check_size': True, 'output': True},
{'string': ('a','b','A'), 'check_size': True, 'output': False},
{'string': {'a':'b','b':'b','a':'a'}, 'check_size': True, 'output': TypeError},
{'string': {'a':'b','c':'c','b':'a'}, 'check_size': True, 'output': TypeError},
{'string': {'ab':'bsdfsdfsdfsdf','ba':'cerwerwerwer'}, 'check_size': True, 'output': TypeError},
{'string': tester_function(), 'check_size': True, 'output': True},
{'string': tester_function, 'check_size': True, 'output': TypeError},
# brak sprawdzania wielkości liter
{'string': 'okO', 'check_size': False, 'output': True},
{'string': 'Oko', 'check_size': False, 'output': True},
{'string': 'kamilŚlimak', 'check_size': False, 'output': True},
{'string': 'Holandia', 'check_size': False, 'output': False},
{'string': '', 'check_size': False, 'output': False},
{'string': ['a','b','A'], 'check_size': False, 'output': True},
{'string': ('a','b','A'), 'check_size': False, 'output': True},
]

count = 0
for test in test_cases:
	count+=1
	print(count)
	expectation = test['output']
	word = test['string']
	try:
		response = is_palindrome(test['string'],test['check_size'])
		assert response == expectation, f'Expected {repr(expectation)} for {repr(word)} got {repr(response)}'
	except TypeError:
		assert expectation == TypeError, f'Expected {repr(expectation)} for {repr(word)} got {TypeError}'