import unittest
from tic_tac_toe.utilities import tic_tac_toe_winner, tic_tac_toe_board_drawer


# UNIT TEST FOT tic_tac_toe_winner
class TestFinishedGame(unittest.TestCase):

    def test_3x_in_a_row(self):
        for i, board in enumerate(('XXXO  O O', 'O  XXXO O', 'O OO  XXX')):
            with self.subTest(row=i + 1):
                self.assertEqual(tic_tac_toe_winner(board), 'X')

    def test_3o_in_a_column(self):
        for i, board in enumerate(('OX O XOX ', 'XOX O XOX', 'X OOXOX O')):
            with self.subTest(col=i + 1):
                self.assertEqual(tic_tac_toe_winner(board), 'O')

    def test_3x_diagonally(self):
        self.assertEqual(tic_tac_toe_winner('XO OX   X'), 'X')


class TestUnfinishedGame(unittest.TestCase):

    def test_empty_board(self):
        self.assertIsNone(tic_tac_toe_winner(' ' * 9))

    def test_only_two_signs(self):
        self.assertIsNone(tic_tac_toe_winner('X O      '))

    def test_6_signs_no_winner(self):
        self.assertIsNone(tic_tac_toe_winner('X OO XX O'))


class TestInvalidBoard(unittest.TestCase):

    def test_illegal_symbols(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_winner('    E    ')

    def test_too_large_board(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_winner('XOOOXXXXOX')

    def test_too_small_board(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_winner('XO  X X')

    def test_bad_number_of_signs(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_winner('X X X    ')

    def test_too_many_x(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_winner('X X X   X')

    def test_too_many_winners(self):
        with self.assertRaises(TypeError):
            tic_tac_toe_winner('XXXOOOXOO')

    # UNIT TEST FOT tic_tac_toe_board_drawer
class TestProperDrawing(unittest.TestCase):
    def test_x_draw_2_1_on_empty(self):
        self.assertEqual(tic_tac_toe_board_drawer('4X'), '   X     ')

    def test_x_draw_2_1_on_board(self):
        self.assertEqual(tic_tac_toe_board_drawer('4X', 'O        '), 'O  X     ')

    def test_o_draw_3_3_on_empty(self):
        self.assertEqual(tic_tac_toe_board_drawer('9O'), '        O')

class TestInvalidBDrawing(unittest.TestCase):

    def test_bad_input_board(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_board_drawer('3X','     ')
    def test_too_long_input_sign(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_board_drawer('30X')
    def test_bad_input_sign(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_board_drawer('3E')

    def test_not_valid_position(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_board_drawer('3X','  O      ')

    def test_sign_on_0_position(self):
        with self.assertRaises(ValueError):
            tic_tac_toe_board_drawer('0X','  O      ')

import doctest
import tic_tac_toe


def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(tic_tac_toe))
    return tests
