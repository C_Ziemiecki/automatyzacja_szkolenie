from itertools import count
from os import close, unlink
from tempfile import mkstemp
from unittest.mock import Mock

import pytest
from sqlalchemy import create_engine
from tic_tac_toe.database import winner, metadata, history


@pytest.fixture
def read_from_file():
    def file_reader(file):
        sequences = [[], []]
        with open(file) as f:
            for line in f:
                input = line.strip().split('-')
                sequences[0].append(input[0])
                sequences[1].append(input[1])
            return sequences

    return file_reader


@pytest.fixture
def create_games(database_connection):
    def game_creator(*games):
        move_id = count(1)
        for game_id, moves in enumerate(games, 1):
            database_connection.execute(history.insert(), [
                {'game_id': game_id, 'move_id': next(move_id), 'position': int(position), 'symbol': symbol} for
                position, symbol in zip(moves[::2], moves[1::2])])

    return game_creator


@pytest.fixture
def database_connection():
    fd, name = mkstemp(prefix='test_winner_', suffix='.sqlite')
    engine = create_engine(f'sqlite:///{name}')
    metadata.create_all(engine)
    with engine.connect() as connection:
        yield connection
    close(fd)
    unlink(name)


@pytest.fixture
def connection_mock():
    return lambda moves: Mock(
        execute=Mock(return_value=[(int(position), symbol) for position, symbol in zip(moves[::2], moves[1::2])]))


@pytest.mark.parametrize("scenario,sign",
                         [('4X1O5X3O2X6O8X', 'X'), ('4O1X5O3X2O6X8O', 'O'), ('1X2O3X4O5X6O', 'O')])
def test_3x_in_a_column(database_connection, create_games, scenario, sign):
    create_games(scenario)
    assert winner(database_connection, 1) == sign


def test_3x_in_a_row(database_connection, create_games, read_from_file):
    game = read_from_file('test/games.txt')
    create_games(*game[0])
    for id, element in enumerate(game[1], 1):
        assert winner(database_connection, id) == element


def test_3x_diagonal(connection_mock, monkeypatch):
    monkeypatch.setattr('tic_tac_toe.database.tic_tac_toe_winner', lambda board: board)
    assert winner(connection_mock('0X2O4X5O8X'), None) == 'X O XO  X'
