from tic_tac_toe.utilities import tic_tac_toe_winner, tic_tac_toe_board_drawer
from tic_tac_toe.api import app
import random

if __name__ == "__main__":
    app.run(debug=True)
    # player = 'X'
    # game_board = ' ' * 9
    #
    # while ' ' in game_board:
    #     place = round(random.random() * 9)
    #     decision = f'{place}{player}'
    #     try:
    #         game_board = tic_tac_toe_board_drawer(decision, game_board)
    #         print(game_board)
    #         if tic_tac_toe_winner(game_board) in ['X','O']:
    #             break
    #         player = 'O' if player == 'X' else 'X'
    #     except:
    #         print('Bad place')
    # print(player)