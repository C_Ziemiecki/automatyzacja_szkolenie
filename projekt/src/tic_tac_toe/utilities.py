def tic_tac_toe_board_drawer(input, board=' ' * 9):
    if len(input)!=2 or len(board)!=9:
        raise ValueError
    order = int(input[0])-1
    sign = input[1]
    if sign not in ['X','O']:
        raise ValueError
    if board[order]!=' ':
        raise ValueError
    if order<0:
        raise ValueError
    return board[:order] + sign + board[order+1:]

def tic_tac_toe_winner(board):
    def check_compatibility(board):
        if isinstance(board, str):
            x_count = board.count('X')
            y_count = board.count('O')
            empty_count = board.count(' ')
            if x_count + y_count + empty_count != 9:
                pass
            elif abs(x_count - y_count) > 1:
                pass
            else:
                return True
        return False

    def check_winners(candidates):
        if len(candidates) == 0 or candidates[0] == ' ':
            return None
        else:
            for sign in range(1, len(candidates)):
                if candidates[0] != candidates[sign]:
                    return 'Error'
        return candidates[0]

    if check_compatibility(board):
        new_board = [board[x:x + 3] for x in range(0, 9, 3)]
        candidates = []
        for x in range(3):
            if new_board[x][0] == new_board[x][1] == new_board[x][2]:
                candidates.append(new_board[x][0])
            if new_board[0][x] == new_board[1][x] == new_board[2][x]:
                candidates.append(new_board[0][x])
        if new_board[0][0] == new_board[1][1] == new_board[2][2]:
            candidates.append(new_board[0][0])
        if new_board[0][2] == new_board[1][1] == new_board[2][0]:
            candidates.append(new_board[2][0])
        winner = check_winners(candidates)
        if winner in ['X', 'O', None]:
            return winner
        raise TypeError()
    raise ValueError()
