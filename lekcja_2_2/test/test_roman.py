from roman.converter import convert, numeral_system
import pytest

def test_empty_string():
    with pytest.raises(ValueError):
       convert('','DECIMAL')

def test_invalid_numeral_system():
    with pytest.raises(ValueError):
       convert('X','DEICMAL')

def test_X_to_decimal():
    assert convert('X','DECIMAL') == '10'

def test_III_to_decimal():
    assert convert('III','DECIMAL') == '3'

def test_IIII_to_decimal():
    with pytest.raises(ValueError):
        convert('IIII','DECIMAL')

def test_DCLXVIII_to_decimal():
    assert convert('DCLXVIII','DECIMAL') == '668'

def test_IV_to_decimal():
    assert convert('IV','DECIMAL') == '4'

def test_CMLIV_to_decimal():
    assert convert('CMLIV','DECIMAL') == '954'

# @pytest.mark.skip(reason="Test will pass with future functions")
def test_bad_strucuture_to_decimal():
    with pytest.raises(ValueError):
        convert('CMDCCIV', 'DECIMAL')

def test_bad_sugn_to_decimal():
    with pytest.raises(ValueError):
        convert('CMh', 'DECIMAL')

def test_3_to_roman():
    assert convert('3','ROMAN') == 'III'

def test_12_to_roman():
    assert convert('12','ROMAN') == 'XII'

def test_3467_to_roman():
    assert convert('3467','ROMAN') == 'MMMCDLXVII'

def test_negative_value_to_roman():
    with pytest.raises(ValueError):
        assert convert('-456','ROMAN') == 'MMMCDLXVII'


def test_binnary_to_roman():
    assert convert('0b1011','ROMAN') == 'XI'

def test_hexadecimal_to_roman():
    assert convert('0xFF','ROMAN') == 'CCLV'

def test_octal_to_roman():
    assert convert('0o66','ROMAN') == 'LIV'

def test_triangal_to_roman():
    assert convert('3s222','ROMAN') == 'XXVI'

def test_pentangal_to_roman():
    assert convert('5s222','ROMAN') == 'LXII'


def test_binnary_to_decimal():
    assert convert('0b1011','DECIMAL') == '11'

def test_hexadecimal_to_decimal():
    assert convert('0xFF','DECIMAL') == '255'

def test_octal_to_decimal():
    assert convert('0o66','DECIMAL') == '54'

def test_triangal_to_decimal():
    assert convert('3s222','DECIMAL') == '26'

def test_pentangal_to_decimal():
    assert convert('5s222','DECIMAL') == '62'

def test_3_to_binary():
    assert convert('3','BINARY') == '0b11'

def test_2_to_hexadecimal():
    assert convert('22','HEXADECIMAL') == '0x16'


def test_X_to_binary():
    assert convert('X', 'BINARY') == '0b1010'


def test_XLVIII_to_hexadecimal():
    assert convert('XLVIII', 'HEXADECIMAL') == '0x30'

def test_XLVIII_to_triangle():
    assert convert('XLVIII', 'TRIANGLE') == '3s1210'