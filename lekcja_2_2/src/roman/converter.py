import math
from enum import Enum


class numeral_system(Enum):
    DECIMAL = 0
    BINARY = 1
    HEXADECIMAL = 2
    ROMAN = 3
    TRIANGLE = 4


class roman_numbers(Enum):
    M = 1000
    CM = 900
    D = 500
    CD = 400
    C = 100
    XC = 90
    L = 50
    XL = 40
    X = 10
    IX = 9
    V = 5
    IV = 4
    I = 1


class system_shortcut(Enum):
    B = 2
    O = 8
    X = 16


def convert(number, system):
    def valid_number(number):
        if number == '':
            raise ValueError()
        return number

    def valid_system(system):
        if any(x for x in numeral_system if x.name == system):
            return True
        raise ValueError()

    def if_in_enum(element, enu):
        return any(x for x in enu if x.name == element)

    def to_decimal(number):
        output_number = 0
        input_number = number
        if if_in_enum(number[0], roman_numbers):
            while len(number) > 0:
                if if_in_enum(number[0:2], roman_numbers):
                    output_number += roman_numbers[number[0:2]].value
                    number = number[2:]
                elif if_in_enum(number[0:1], roman_numbers):
                    output_number += roman_numbers[number[0:1]].value
                    number = number[1:]
                else:
                    raise ValueError()
            if to_roman(output_number) != input_number:
                raise ValueError()
        else:
            if len(number) > 2:
                if if_in_enum(number[1].upper(), system_shortcut):
                    output_number = int(number, system_shortcut[number[1].upper()].value)
                elif number[1] == 's':
                    output_number = int(number[2:], int(number[0]))
                else:
                    output_number = int(number)
            else:
                output_number = int(number)
        return output_number

    def to_roman(number):
        output_number = ''
        while number > 0:
            for roman in roman_numbers:
                if number >= roman.value:
                    number -= roman.value
                    output_number += roman.name
                    break
        return output_number

    def to_triangle(number):
        output_number = ''
        while number > 0:
            output_number = str(number % 3) + output_number
            number = math.floor(number / 3)
        return '3s' + output_number

    function_map = {
        'DECIMAL': to_decimal,
        'ROMAN': to_roman,
        'BINARY': bin,
        'HEXADECIMAL': hex,
        'TRIANGLE': to_triangle
    }

    valid_system(system)
    number = function_map['DECIMAL'](valid_number(number))
    if system != 'DECIMAL': number = function_map[system](number)
    return valid_number(str(number))
