import math

def prime_factors(number):
    if not isinstance (number,(int,tuple)):
        raise TypeError('Input value is not single integer')
    if hasattr(number, '__iter__') and len(number)>1:
        raise ValueError('Input value contains more numbers than one')
    if number > 100000000000000:
        raise ValueError('Value is too big')
    if number<2:
        return None
    primes = set()
    while number % 2 == 0:
        primes.add(2)
        number /= 2
    for candidate in range(3,int(math.sqrt(number))+1,2):
        while number % candidate== 0:
            primes.add(candidate)
            number /= candidate
    if number > 2:
        primes.add(number)
    return sorted(primes)


def test_zero_value():
    assert prime_factors(0) is None

def test_one():
    assert prime_factors(1) is None

def test_negative_value():
    assert prime_factors(-2) is None

def test_prime_value():
    primes = prime_factors(7)
    assert primes == [7], f'expected [7] got {primes}'

def test_positive_value1():
    primes = prime_factors(15)
    assert primes == [3,5], f'expected [3,5] got {primes}'

def test_positive_value2():
    primes = prime_factors(4950)
    assert primes == [2,3,5,11], f'expected [2,3,5,11] got {primes}'


def test_positive_value3():
    try:
        primes = prime_factors(4950342374623042384168469)
        assert False, "ValueError expected"
    except ValueError:
        pass

def test_positive_value4():
    primes = prime_factors(4950342374239)
    assert primes == [337, 14689443247.0], f'expected [337, 14689443247.0] got {primes}'

def test_positive_even_value():
    primes = prime_factors(16)
    assert primes == [2], f'expected [2] got {primes}'

def test_string_number():
    
    try:
        primes = prime_factors('12')
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_string_text():
    try:
        primes = prime_factors('ala ma kota')
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_float():
    try:
        primes = prime_factors(124.4)
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_one_element_list():
    try:
        primes = prime_factors([28])
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_multi_element_list():
    try:
        primes = prime_factors([28,5,645,324,141])
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_one_element_tuple():
    try:
        primes = prime_factors((28))
        assert primes == [2,7], f'expected [2,7] got {primes}'
    except TypeError:
        pass

def test_multi_element_tuple():
    try:
        primes = prime_factors((28,13))
        assert False, "ValueError expected"
    except ValueError:
        pass


def test_single_element_dict():
    try:
        primes = prime_factors({'one':28})
        assert False, "TypeError expected"
    except TypeError:
        pass

def test_single_element_set():
    try:
        primes = prime_factors({28})
        assert False, "TypeError expected"
    except TypeError:
        pass


if __name__ == '__main__':
    for test in (
        test_zero_value,
        test_negative_value,
        test_positive_value1,
        test_positive_value2,
        test_positive_value3,
        test_positive_value4,
        test_positive_even_value,
        test_string_text,
        test_string_number,
        test_one,
        test_float,
        test_one_element_list,
        test_multi_element_list,
        test_one_element_tuple,
        test_multi_element_tuple,
        test_single_element_dict,
        test_single_element_set,
        test_prime_value
    ):  #➜ 4
        print(f'{test.__name__}: ', end='')
        try:
            test()
            print('OK')
        except AssertionError as error:
            print(error)